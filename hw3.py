# -*- coding: utf-8 -*-
"""
Created on Thu Oct 09 20:56:11 2014

@author: Yifan
"""

import matplotlib

matplotlib.use('TkAgg')

from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg

import matplotlib.pyplot as plt
import scipy.integrate as integrate
import matplotlib.animation as animation
from numpy import *
from Tkinter import *

# deg - rad convertor

rad = pi/180.0

class SinglePendulum():
    
    global rad
    
    def __init__(self):
        
        self.l = 1.0 # m
        self.m = 10 # kg
        self.i = 1 # kg*m^2
        self.g = 9.81 # m/s^2
            
        self.theta = 90.0 
        self.theta_dot = 0.0 
        self.t = arange(0.0, 20, 0.005)
        
    def InitializePhysics(self):
        
        self.theta = self.theta *rad
        self.theta_dot = self.theta_dot * rad
        
        self.ini = array([self.theta , self.theta_dot])    
        
        # solver 
    def ConstructSolution(self):
        
        self.output = integrate.odeint(self.system, self.ini, self.t)
                     
    def system(self, state, t):
        
        self.x = zeros_like(state)
        self.x[0]=state[1]
        self.x[1]=-(self.m*self.g*self.l*sin(state[0]))/(self.m*self.l*self.l+self.i)
        
        return self.x

    def Plot(self):
        
        self.Fig = plt.figure(figsize=(3,3))
        
        self.pp = self.Fig.add_subplot(111, xlim = (0,max(self.t)), ylim=(-400,400))
        
        self.pp.plot(self.t, (self.output)*180.0/pi)
 
        self.pp.set_xlabel('Time (sec)')
        self.pp.set_ylabel('THETA (deg) & THETA_DOT (deg/sec)')
        
        self.pp.grid()
##        
    def Animation(self):
            
        self.x = self.l*sin(self.output[:,0])
        self.y = -self.l*cos(self.output[:,0])
        
        self.fig = plt.figure(figsize = (3,3))
        self.f = self.fig.add_subplot(111, xlim = (-2,2), ylim = (-2,2))
        self.f.grid()
        self.line, = self.f.plot([], [], '-bo', lw = 2)
            
        
        self.cartoon = animation.FuncAnimation(self.fig, self.anim, arange(1, len(self.output)), interval=0.1, blit=True, init_func = self.init)
        
    def init(self):

        self.line.set_data([], [])
        return self.line,
        
    def anim(self, i):
        
            self.X = [0, self.x[i]]   
            self.Y = [0, self.y[i]]    
            self.line.set_data(self.X, self.Y)  
            return self.line,
            
# end of class SinglePendulum
            
# class: Double Pendulum
class DoublePendulum():
    
    global rad
    
    def __init__(self):
        
        # set default values
        
        self.m1 = 1.0
        self.m2 = 1.0
        self.l1 = 1.0
        self.l2 = 1.0
        self.g = 9.81
        
        # define initial conditions
        
        self.theta1 = 90.0 
        self.theta1_dot = 0.0 
        self.theta2 = 70.0
        self.theta2_dot = 0.0
        
        # define timespan
        
        self.t = arange(0.0, 30, 0.001)
        
    def InitializePhysics(self):
        
        self.theta1 = self.theta1 * rad
        self.theta1_dot = self.theta1_dot* rad
        self.theta2 = self.theta2* rad
        self.theta2_dot = self.theta2_dot * rad
        
        self.ini = array([self.theta1, self.theta1_dot, self.theta2, self.theta2_dot])  

        # solver
    def ConstructSolution(self):

        self.output = integrate.odeint(self.sys, self.ini, self.t)           
            
        
    def sys(self, state, t):
        
        self.x = zeros_like(state)
        
        self.delta = state[2]-state[0]
        
        self.x[0] = state[1]
        
        self.x[1] = (self.m2*self.l1*state[1]*state[1]*sin(self.delta)*cos(self.delta)
    
                       + 2*self.g*sin(state[2])*cos(self.delta) + self.m2*self.l2*state[3]*state[3]*sin(self.delta)
               
                       - (self.m1+self.m2)*self.g*sin(state[0]))/((self.m1+self.m2)*self.l1 - self.m2*self.l1*cos(self.delta)*cos(self.delta))

        
        self.x[2] = state[3]
        
        self.x[3] = (-self.m2*self.l2*state[3]*state[3]*sin(self.delta)*cos(self.delta)
     
                       + (self.m1+self.m2)*self.g*sin(state[0])*cos(self.delta)
                   
                       - (self.m1+self.m2)*self.l1*state[1]*state[1]*sin(self.delta)
               
                       - (self.m1+self.m2)*self.g*sin(state[2]))/(((self.m1+self.m2)*self.l1 - self.m2*self.l1*cos(self.delta)*cos(self.delta))*self.l2/self.l1)
                        
        return self.x
#        
    def Plot(self):
        
        self.Fig = plt.figure(figsize=(3,3))
        
        self.pp = self.Fig.add_subplot(111, xlim = (0,max(self.t)), ylim=(-400,400))
        
        self.pp.plot(self.t, (self.output)*180.0/pi)
 
        self.pp.set_xlabel('Time (sec)')
        self.pp.set_ylabel('THETA (deg) & THETA_DOT (deg/sec)')
        
        self.pp.grid()

        
    def Animation(self):
        
        self.x1 = self.l1 * sin(self.output[:, 0])
        self.y1 = -self.l1 * cos(self.output[:, 0])
        self.x2 = self.l2 * sin(self.output[:,2]) + self.x1
        self.y2 = -self.l2 * cos(self.output[:,2]) + self.y1       
        
        self.fig = plt.figure(figsize=(3,3))
        self.f = self.fig.add_subplot(111, xlim = (-3,3), ylim = (-3,3))
        self.f.grid()
        self.line, = self.f.plot([], [], '-bo', lw = 2)
        
        self.cartoon = animation.FuncAnimation(self.fig, self.anim, arange(1, len(self.output)), interval=0.01, blit=True, init_func = self.init)
        
#        plt.show()        
        
    def init(self):

        self.line.set_data([], [])
        return self.line,
        
    def anim(self, i):
        
        self.X = [0, self.x1[i], self.x2[i]]   
        self.Y = [0, self.y1[i], self.y2[i]]    
        self.line.set_data(self.X, self.Y)  
        return self.line,
        
# end of class DoublePendulum
        
# start of class: Application

class Application():
    
    def GeneratePendulum1(self):
        
        self.p = SinglePendulum()

        print "A new model for simulation has been constructed. Choose either 'Plot' or 'Animation' to see the result.\n Use STOP button to quit."

        self.genePeIsCalled1 = True
        
        
        


    def GeneratePendulum2(self):
        
        self.p = DoublePendulum()
        
        print "A new model for simulation has been constructed. Choose either 'Plot' or 'Animation' to see the result.\n Use STOP button to quit."
    
        self.genePeIsCalled2 = True
        
    def Initialization1(self):
        
        self.p.m = float(self.e1.get())
        self.p.l = float(self.e2.get())
        self.p.i = float(self.e3.get())
        self.p.theta = float(self.e4.get())
        self.p.g = float(self.GravityC.get( ))     
        self.p.t = arange(0.0, self.v.get(), 0.001)   
        self.p.InitializePhysics()
        self.p.ConstructSolution()
        self.p.Plot()
        self.p.Animation()

    def Initialization2(self):
        
        self.p.m1 = float(self.e1.get( ))
        self.p.m2 = float(self.e2.get( ))
        self.p.l1 = float(self.e3.get( ))
        self.p.l2 = float(self.e4.get( ))
        self.p.theta1 = float(self.e5.get( )) - 1.0
        self.p.theta2 = float(self.e6.get( ))
        self.p.g = float(self.GravityC.get( ))      
        self.p.t = arange(0.0, self.v.get(), 0.001)
        self.p.InitializePhysics()
        self.p.ConstructSolution()
        self.p.Plot()
        self.p.Animation()
        
    
    def PLOT1(self):
        
        if(self.genePeIsCalled1 == True):
            
            self.Initialization1()            
            
            self.canvas1 = FigureCanvasTkAgg(self.p.Fig, self.root1)
            self.canvas1.get_tk_widget().grid(row = 11, column = 1)     
            
            self.canvas1.show()
            
        else:
            
            print "You need to create the model first!"
        
        
    
    def PLOT2(self):
        
        if(self.genePeIsCalled2 == True):
            
            self.Initialization2()            
            
            self.canvas1 = FigureCanvasTkAgg(self.p.Fig, self.root2)
            self.canvas1.get_tk_widget().grid(row = 11, column = 1)     
            
            self.canvas1.show()        
      
        else:
          
            print "You need to create the model first!"
            
    def ANIMATION1(self):
        
        if(self.genePeIsCalled1 == True):
            
            self.Initialization1()
                  
            self.canvas2 = FigureCanvasTkAgg(self.p.fig, self.root1)
            self.canvas2.get_tk_widget().grid(row = 11, column = 4)     
            
            self.canvas2.show()
            
        else:
            
            print "You need to create the model first!"
            
    def ANIMATION2(self):
        
        if(self.genePeIsCalled2 == True):
            
            self.Initialization2()
                  
            self.canvas2 = FigureCanvasTkAgg(self.p.fig, self.root2)
            self.canvas2.get_tk_widget().grid(row = 11, column = 4)     
            
            self.canvas2.show()     
      
        else:
          
            print "You need to create the model first!"
            
    def createWidgets1(self):
        
        self.root1 = Tk()
        
        # system parameters
        
        Label(self.root1, text = "M: ").grid(row =0 , column = 0, padx = 5, pady =5)
        Label(self.root1, text = "L: ").grid(row =1 , column = 0, padx = 5, pady =5)
        Label(self.root1, text = "I: ").grid(row =2 , column = 0, padx = 5, pady =5)      
        
        # initial condictions
        
        Label(self.root1, text = "Theta ").grid(row =3 , column = 0, padx = 5, pady =5) 
        Label(self.root1, text = "Simulation TIme:  ").grid(row =4 , column = 0, padx = 5, pady =5) 
        Label(self.root1, text = "Gracity Constant: ").grid(row =5 , column = 0, padx = 5, pady =5)
        
        # Entries
        
        self.e1 = Entry(self.root1)
        self.e1.insert(10,2.0)
        self.e1.grid(row=0, column=1)
         
        self.e2 = Entry(self.root1)
        self.e2.insert(10,1.0)
        self.e2.grid(row=1, column=1)
         
        self.e3 = Entry(self.root1)
        self.e3.insert(10,0.025)
        self.e3.grid(row=2, column=1)
         
        self.e4 = Entry(self.root1)
        self.e4.insert(10,80.0)
        self.e4.grid(row=3, column=1)
        
        # Buttons
        
        Button(self.root1, text = "Run Simulation",                                
                              fg = "blue",                                                                
                              command = self.GeneratePendulum1).grid(row = 0, column = 4, padx =60, pady = 5)
                              
        Button(self.root1, text = "Plot Result",                              
                              fg = "magenta",                                                                  
                              command = self.PLOT1).grid(row = 1, column = 4, padx =60,  pady = 5)
                                                            
        Button(self.root1, text = "Animation",                  
                       fg = "orange",                   
                       command = self. ANIMATION1). grid(row = 2, column = 4, padx =60)
                       
        Button(self.root1, text ="STOP", fg = "red", width = 10, command = self.root1.destroy).grid( row = 3, column = 4,padx = 60, pady = 10 )
        
        
         
        # optionList
         
        self.optionList = (10.0 , 20.0 , 30.0, 40.0 )

        self.v = DoubleVar()

        self.v.set(self.optionList[1])

        self.m = OptionMenu(self.root1, self.v, *self.optionList)
         
        self.m.config(width=20)
         
        self.m.grid(row = 4, column = 1, pady = 10)
        
        self.v.trace('w', self.hasChanged)
         
         
        # Slider 
         
        self.GravityC = Scale(self.root1, from_=0.0, to=20.0, length = 200, resolution = 0.1,orient = HORIZONTAL)
        self.GravityC.set(9.8)
        self.GravityC.grid(row = 5,  column = 1, sticky = W+S+E+N)
        
        self.interation_x = []
        
        self.interation_y = []
        
        self.count = 1
        
        self.interation_x.append(self.count)
        
        self.interation_y.append(self.v.get())
        
        self.ffiigg = plt.figure(figsize = (4,4))
        
        self.b = self.ffiigg.add_subplot(111, xlim = (0, 40), ylim=(0, 50))
        
        self.b.plot(self.interation_x, self.interation_y,'-ro' )
        
        self.b.grid()
        
        self.canvas3 = FigureCanvasTkAgg(self.ffiigg, self.root1)
        
        self.canvas3.get_tk_widget().grid(row = 11, column = 7)     
            
        self.canvas3.show()
        
        
        
        
    def createWidgets2(self):
        
         self.root2 = Tk()
         
         # System Parameters
        
         Label(self.root2, text="M1: ").grid(row=0, column = 0 ,padx=5, pady=5)
         Label(self.root2, text="M2: ").grid(row=1, column = 0 ,padx=5, pady=5)
         Label(self.root2, text="L1: ").grid(row=2, column = 0 , padx=5, pady=5)
         Label(self.root2, text="L2: ").grid(row=3, column = 0 , padx=5, pady=5)   
         
         # Initial Conditions
         
         Label(self.root2, text="Theta1: ").grid(row=4, column = 0, padx=5, pady=5 ) 
         Label(self.root2, text="THeta2: ").grid(row=5, column = 0, padx=5, pady=5 ) 
         Label(self.root2, text="Gravity Constant: ").grid(row=9, column = 0 )
         Label(self.root2, text="Simulation Time: ").grid(row=7, column = 0 )          
         
         
         # Entries
        
         self.e1 = Entry(self.root2)
         self.e1.insert(10,2.0)
         self.e1.grid(row = 0, column = 1)
         self.e2 = Entry(self.root2)
         self.e2.insert(10,2.0)
         self.e2.grid(row = 1, column = 1)
         self.e3 = Entry(self.root2)
         self.e3.insert(10,1.0)
         self.e3.grid(row = 2, column = 1)
         self.e4 = Entry(self.root2)
         self.e4.insert(10,1.0)
         self.e4.grid(row = 3, column = 1)
         self.e5 = Entry(self.root2)
         self.e5.insert(10,70.0)
         self.e5.grid(row = 4, column = 1)
         self.e6 = Entry(self.root2)
         self.e6.insert(10,60.0)
         self.e6.grid(row = 5, column = 1)    
         
         # Buttons
         
         Button(self.root2, text = "Run Simulation",                                
                              fg = "blue",                                                                
                              command = self.GeneratePendulum2).grid(row = 0, column = 4, padx =60, pady = 5)
                              
         Button(self.root2, text = "Plot Result",                              
                              fg = "magenta",                                                                  
                              command = self.PLOT2).grid(row = 1, column = 4, padx =60,  pady = 5)
                                                            
         Button(self.root2, text = "Animation",                  
                       fg = "orange",                   
                       command = self. ANIMATION2). grid(row = 2, column = 4, padx =60)
                       
         Button(self.root2, text ="STOP", fg = "red", width = 10, command = self.root2.destroy).grid( row = 3, column = 4,padx = 60, pady = 10 )
          
         
            
         # Slider
                       
         self.GravityC = Scale(self.root2, from_=0.0, to=20.0, length = 200, resolution = 0.1,orient = HORIZONTAL)
         self.GravityC.set(9.8)
         self.GravityC.grid(row = 9,  column = 1, sticky = W+S+E+N)
         
         # Drop - down list
         
         self.optionList = (10.0 , 20.0 , 30.0, 40.0 )

         self.v = DoubleVar()

         self.v.set(self.optionList[0])

         self.m = OptionMenu(self.root2, self.v, *self.optionList)
         
         self.m.config(width=20)
         
         self.m.grid(row = 7, column = 1, pady = 10)
         
         self.v.trace('w', self.hasChanged)
         
         self.interation_x = []
        
         self.interation_y = []
        
         self.count = 1
        
         self.interation_x.append(self.count)
        
         self.interation_y.append(self.v.get())
        
         self.ffiigg = plt.figure(figsize = (4,4))
        
         self.b = self.ffiigg.add_subplot(111, xlim = (0, 40), ylim=(0, 50))
        
         self.b.plot(self.interation_x, self.interation_y,'-ro' )
        
         self.b.grid()
        
         self.canvas3 = FigureCanvasTkAgg(self.ffiigg, self.root2)
        
         self.canvas3.get_tk_widget().grid(row = 11, column = 7)     
            
         self.canvas3.show()
         
    def hasChanged(self,*args):
        
        self.count+=1
        
        self.interation_y.append(self.v.get())
        
        self.interation_x.append(self.count)
        
        self.b.plot(self.interation_x, self.interation_y,'-ro' )
        
        self.canvas3.show()
        

    def __init__(self):
        
        self.root = Tk()
        
        self.root.geometry("190x0")
        
        self.menu = Menu(self.root)

        self.root.config(menu = self.menu)

        self.filemenu = Menu(self.menu)

        self.menu.add_cascade(label = "Choose Pendulum Type", menu = self.filemenu)
        
        self.filemenu.add_command(label = "Single Pendulum", command = self.createWidgets1)
        
        self.filemenu.add_command(label = "Double Pendulum", command = self.createWidgets2)
  
        self.genePeIsCalled2 = False
        
        self.genePeIsCalled1 = False
        
    def run(self):
        
        self.root.mainloop()
        
# main body

app = Application()

app.run()